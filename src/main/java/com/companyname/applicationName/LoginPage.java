package com.companyname.applicationName;

import com.companyname.common.SeleniumBasePage;
import org.openqa.selenium.By;

public class LoginPage extends SeleniumBasePage{

    private final By userNameTextBox = By.name("q");

    public void loginToApplication(String url, String value){
        getURL(url);
        enterText(userNameTextBox, value);
    }

}
