package com.companyname.common;

import com.companyname.utilities.DriverManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;

public class SeleniumBasePage extends DriverManager {

    public final static Logger LOGGER  = Logger.getLogger(SeleniumBasePage.class);

    public void getURL(String url){
        LOGGER.info("Before getURL with URL: "+url);
        getDriverInstance().get(url);
        LOGGER.info("After getURL with URL: "+url);
    }

    public void enterText(By locator, String value){
        getDriverInstance().findElement(locator).sendKeys(value);
    }




}
