package com.companyname.utilities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeMethod;

public class DriverManager {

    @BeforeMethod(alwaysRun = true)
    public void newDriverInstance(){
        WebDriver driver = null;
        String browser = System.getProperty("browser");

        if(browser.equalsIgnoreCase("chrome")){
            System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"/src/main/resources/drivers/chromedriver.exe");
            driver = new ChromeDriver();
        }

        Long threadId = Thread.currentThread().getId();
        TestUtil.driverInstanceMap.put(threadId, driver);
    }

    public WebDriver getDriverInstance(){
        Long threadId = Thread.currentThread().getId();
        return TestUtil.driverInstanceMap.get(threadId);
    }

    public void quitDriver(){
        WebDriver driver = getDriverInstance();
        driver.close();
        driver.quit();
    }
}
